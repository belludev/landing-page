import React, { Component } from 'react'

import Navigation from "./layout/Navigation";
import Header from "./layout/Header";
import Projects from "./layout/Projects";
import Experience from "./layout/Experience";
import ReachOut from "./layout/ReachOut";
import Footer from "./layout/Footer";

import { loadReCaptcha } from 'react-recaptcha-v3';
let clientID = '6Lel0toUAAAAAM0zancOQ5pEFtElANGOeoKb02tQ';


export default class App extends Component {
  componentDidMount() {
    loadReCaptcha(clientID);
  }
  render() {
    return (
      <React.Fragment>
        <Navigation />
        <Header />
        <Projects />
        <Experience />
        <ReachOut />
        <Footer />
      </React.Fragment>
    )
  }
}
