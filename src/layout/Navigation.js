import React, { Component } from "react";
import AnchorLink from 'react-anchor-link-smooth-scroll';

export default class Navigation extends Component {
  constructor() {
    super();
    this.state = {
      shrink: false,
      menu: false
    };
  }
  componentDidMount() {
    document.addEventListener("scroll", () => {
      const shrink = window.scrollY > 100;
      if (shrink !== this.state.shrink) {
        this.setState({ shrink });
      }
    });
  }
  toggleMenu = () => {
    let { menu } = this.state;
    this.setState({
      menu: !menu
    });
  };
  closeMenu = () => {
    this.setState({  menu: false })
  }
  render() {
    let { menu } = this.state;
    return (
      <React.Fragment>
        <nav className={`${this.state.shrink ? "shrink" : ""} ${menu ? 'shown' : ''}`}>
          <div className="wrapper">
            <img
              src="/img/logo.svg"
              alt="Bellu Development Logo"
              className="logo"
            />
            <button className={`menu ${menu ? 'close' : ''}`} onClick={() => this.toggleMenu()} />
            <ul className={`navigation ${!menu ? 'hidden' : ''}`}>
              <li>
                <AnchorLink href="#me" onClick={this.closeMenu}>me</AnchorLink>
              </li>
              <li>
                <AnchorLink href="#featured" offset="100" onClick={this.closeMenu}>featured</AnchorLink>
              </li>
              <li>
                <AnchorLink href="#experience" onClick={this.closeMenu}>experience</AnchorLink>
              </li>
              <li>
                <AnchorLink href="#reachout" onClick={this.closeMenu}>reach out</AnchorLink>
              </li>
            </ul>
          </div>
        </nav>
        <div className={`nav-spacing`} />
      </React.Fragment>
    );
  }
}
