import React, { Component } from "react";
import axios from 'axios';

import { ReCaptcha } from 'react-recaptcha-v3';

let clientID = '6Lel0toUAAAAAM0zancOQ5pEFtElANGOeoKb02tQ';

export class ReachOut extends Component {
  constructor(props){
    super(props);
    this.state = {
      buttonDisabled: false,
      fullName: '',
      email: '',
      reasoning: ''
    }
  }
  handleContactForm = (event) => {
    event.preventDefault();
    let { fullName, email, reasoning } = this.state
    this.setState({
      buttonDisabled: true
    })
    window.grecaptcha.execute(clientID, { action: 'homepage' })
      .then(token => {
        axios.post('https://us-central1-andrew-bellucci-portfolio.cloudfunctions.net/send-email', {
            body: {
              'g-recaptcha-response': token,
              fullName,
              email,
              reasoning
            }
          })
          .then(res => {
            console.log(res);
            let { success } = res.data;
            
            this.setState({
              buttonDisabled: false
            })
          })
          .catch(err => {
            console.log(err);
            this.setState({
              buttonDisabled: false
            })
          })
      })
  }
  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }
  render() {
    let { buttonDisabled } = this.state;
    return (
      <div id="reachout" className="reachout-section">
        <div className="wrapper">
          <h2>reach out</h2>
          <div className="contact-form">
            <div className="hot-links">
              <a href="mailto:andrew@bellucci.tech" className="btn">
                <div className="icon mail" />
                <span>andrew@bellucci.tech</span>
              </a>
              <a href="https://twitter.com/AndrewBellucci" className="btn" target="_blank" rel="noreferrer noopener">
                <div className="icon twitter" />
                <span>@belludev</span>
              </a>
              <a href="https://www.linkedin.com/in/andrew-bellucci-8116a4170" className="btn" target="_blank" rel="noreferrer noopener">
                <div className="icon linkedin" />
                <span>Andrew Bellucci</span>
              </a>
            </div>
            <form onSubmit={this.handleContactForm}>
              <div className="grid">
                <input type="text" placeholder="Full Name" name="fullName" onChange={this.onChange} />
                <input type="email" placeholder="eMail" name="email" onChange={this.onChange} />
                <textarea
                  name="reasoning"
                  id=""
                  cols="30"
                  rows="10"
                  placeholder="Reason for reaching out to me"
                  onChange={this.onChange} 
                ></textarea>
              </div>
              <button type="submit" className={`btn send ${buttonDisabled ? 'sending' : ''}`} onClick={() => this.handleContactForm} disabled={buttonDisabled}>
                <div className="icon send" />
                <span>{buttonDisabled ? 'Sending...' : 'Send Message'}</span>
              </button>
            </form>
          </div>
        </div>
        
      </div>
    );
  }
}

export default ReachOut;
