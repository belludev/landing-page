import React, { Component } from 'react'
import { Animated } from "react-animated-css";

export default class Header extends Component {
  constructor(){
    super();
    this.state = {
      about: true
    }
  }
  toggleAbout = () => {
    let { about } = this.state;
    this.setState({
      about: !about
    });
  };
  render() {
    let { about } = this.state;
    return (
      <React.Fragment>
      <header id="me">
        <div className="wrapper">
          <div className="pages">
            <Animated animationIn="fadeInLeft" animationOut="fadeOutLeft" animationInDuration={200} animationOutDuration={200} isVisible={about}>
                <div className="cta">
                  <h1>
                    <small>hi, the name's</small>
                    <div className="name">andrew.</div>
                    <small className="secondary">
                      i'm not too great at design, but i sure can code!
                    </small>
                  </h1>
                  <button onClick={() => this.toggleAbout()}>about</button>
                </div>
            </Animated>
            <Animated animationIn="fadeInLeft" animationOut="fadeOutLeft" animationInDuration={200} animationOutDuration={200} isVisible={!about}>
                <div className="cta">
                  <small className="secondary">
                    Jokes aside, my name is Andrew Bellucci. I'm a developer and aspiring entrepreneur. I love tackling projects and helping people get from Point A to Point B. Be sure to check out my featured work down below and if you like what you see or want help with your project, contact me!
                  </small>
                  <button className="back" onClick={() => this.toggleAbout()}>back</button>
                </div>
            </Animated>
          </div>
        </div>
      </header>
    </React.Fragment>
    )
  }
}
