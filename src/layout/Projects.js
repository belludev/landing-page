import React, { Component } from "react";
import Project from "../components/Project";

export default class Projects extends Component {
  render() {
    let projects = [
      {
        title: 'Preme Profits',
        release: 'September 13th, 2019', 
        meta: [
          {
            color: 'orange', text: 'website'
          },
          {
            color: 'purple', text: 'client'
          }
        ], 
        thumbnail: './img/projects/preme-profits.png',
        modal: {
          description: 'Preme Profits is yet another company that was in need of a professional website to be built. I was delivered a design and a date to complete the website by. Their goal in mind was to have something present their product to future potential customers and I achieved this by using HTML, CSS, and JS with React.js as a library of choice.',
          live: 'https://preme-profits.com'
        }
      },
      {
        title: 'Midnight Proxies',
        release: 'December 13th, 2019', 
        meta: [
          {
            color: 'orange', text: 'website'
          },
          {
            color: 'purple', text: 'client'
          },
          {
            color: 'orange', text: 'backend'
          }
        ], 
        thumbnail: './img/projects/midnight-proxies.png',
        modal: {
          description: 'Midnight Proxies was a company in need of something that wasn\'t just functional but also looked great. They needed a dashboard for their users to create proxies, authorize IP addresses and view their current usages as well as a whole backend system to manage their users. This projected was pieced together with React.js, SASS, Node.js, express.js and much more.',
          testimonials: [
            {
              testimonial: "We arrived at the point of our company’s inception where a Software Developer was required. We had ideas and a list of features we wanted included in our finished product, but the implementation of our vision was unobtainable for our skill set. Andrew came in, and not only brought our vision to life, but was able to provide even more features that we hadn’t even thought of. His ability to immediately integrate himself amongst our team, develop our now finished product, and become someone we couldn’t imagine working without is second to none. Nothing but good things to say from us here at Midnight Proxies.",
              author: "Midnight Proxies, Co-Owner, Matty"
            }
          ],
          live: 'https://midnightproxies.com'
        }
      },
      {
        title: 'Class Pass',
        release: 'August 28th, 2019', 
        meta: [
          {
            color: 'orange', text: 'website'
          },
          {
            color: 'purple', text: 'backend'
          },
          {
            color: 'orange', text: 'design'
          }
        ], 
        thumbnail: './img/projects/class-pass.png',
        modal: {
          description: 'ClassPass was a senior project back in college. A few others and I decided to create a classroom management software that was extremely intuitive for both the teacher and the student to use. Everything was where you would expect it to be. I was given the role to design and develop the frontend as well as develop some of the backend for this service. One day I plan on redesigning the whole system from front to back to see how I can compare my skills then to what they are at the time of building the new system. This project was the reason I was picked up React.js and learned about systems design. I ended up using React.js, Node.js and express.js for the whole system.',
          live: 'https://preme-profits.com'
        }
      }
    ]
    return (
      <div id="featured" className="projects-section">
        <div className="wrapper">
          <h2>
            featured <br />
            projects &amp; work
          </h2>
          <div className="projects">
            {projects.map( project => (
              <Project 
                {...project}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}
