import React from "react";

export default function Footer() {
  return (
    <footer>
      <img src="/img/logo.svg" alt="Bellu Development Logo" />
    </footer>
  );
}
