import React from "react";

import Exp from "../components/Exp";

export default function Experience() {
  return (
    <div id="experience" className="experience-section">
      <div className="wrapper">
        <h2>experience</h2>
        <div className="experiences">
          <Exp color="orange" startDate="01/20/2009" skill="html" />
          <Exp color="purple" startDate="01/20/2009" skill="css" />
          <Exp color="purple" startDate="10/10/2014" skill="javascript" />
          <Exp color="orange" startDate="12/01/2014" skill="sass" />
          <Exp color="orange" startDate="06/12/2016" skill="react.js" />
          <Exp color="purple" startDate="07/18/2016" skill="node.js" />
          <Exp color="purple" startDate="07/18/2016" skill="express.js" />
          <Exp color="orange" startDate="02/27/2015" skill="php" />
          <Exp color="orange" startDate="01/15/2017" skill="c#" />
          <Exp color="purple" startDate="09/26/2016" skill="c++" />
        </div>
      </div>
    </div>
  );
}
