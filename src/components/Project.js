import React, { Component } from 'react'

export default class Project extends Component {
  constructor(props){
    super(props);
    this.state = {
      modalState: 'hidden',
      modalStateClass: ''
    }
  }
  showModal = () => {
    this.setState({
      modalState: 'visible'
    }, () => {
      setTimeout(() => {
        this.setState({
          modalStateClass: 'shown'
        })
      }, 1)
    })
  }
  hideModal = () => {
    this.setState({
      modalStateClass: 'hidden'
    }, () => {
      setTimeout(() => {
        this.setState({
          modalState: ''
        })
      },200)
    })
  }
  render() {
    let { modalState, modalStateClass } = this.state;
    let { meta, title, release, thumbnail, modal } = this.props;
    return (
      <React.Fragment>
        <div className="project" onClick={this.showModal}>
          <div
            className="project-head"
            style={{ backgroundImage: `url(${thumbnail})` }}
          >
            <div className="meta">
              {meta ? meta.map(m => (
                <div className={`meta-info ${m.color}`}>{m.text}</div>
              )) : null}
            </div>
          </div>
          <div className="text">
            <h3>{title}</h3>
            <p className="release-date">{release}</p>
          </div>
        </div>
        {modalState === 'visible' ? (
          <div className={`project-modal ${modalStateClass}`}>
            <div className="content">
              <h3>{title}</h3>
              <div className="modal-content">
                <p>{modal.description}</p>
                {modal.testimonials ? (
                  modal.testimonials.map( testimonial => (<p className="testimonial">{testimonial.testimonial}<span>- {testimonial.author}</span></p>))
                ) : null }
              </div>
              {modal.live ? (<a href={modal.live} target="_blank" rel="noreferrer noopener" className="btn corner"><div className="icon live"/><span>Live Preview</span></a>) : null }
              <button onClick={this.hideModal} className="close">&times;</button>
            </div>
          </div>
        ) : null}
        
      </React.Fragment>
    )
  }
}
