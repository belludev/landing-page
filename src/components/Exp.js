import React from "react";

export default function Exp(props) {
  let theBeginning = "01/01/2009";
  let percentage =
    (new Date(theBeginning).getTime() / new Date(props.startDate).getTime()) *
    89;
  return (
    <div className="experience">
      <div className="exp-level">
        <div
          className={`level ${props.color}`}
          style={{ width: `${percentage}%` }}
        >
          <p>
            {new Date(Date.now()).getFullYear() -
              new Date(props.startDate).getFullYear()}{" "}
            Years
          </p>
        </div>
      </div>
      <h3>{props.skill}</h3>
    </div>
  );
}
